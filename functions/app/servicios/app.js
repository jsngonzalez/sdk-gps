exports.appnueva = function(req, res, db, token) {

    console.log("nueva app para el usuario:",token);
    const uuidv1 = require('uuid/v1');

    const newapp = req.body.data;
    newapp.appid = uuidv1();
    newapp.uid = token;
    newapp.packageNameAndroid = "";
    newapp.packageNameiOS = "";
	newapp.reg = new Date();

    const docRef = db.collection("users").doc(token);
	return docRef.get().then(function(doc) {
		
		if (doc.exists) {
			console.log("data:", doc.data());
            const docApps = db.collection("applications").doc(newapp.appid);
            return docApps.set(newapp).then(function(){

                var appUser = newapp;
                delete appUser.packageNameAndroid
                delete appUser.packageNameiOS
                delete appUser.uid

                appUser.admin = true

                return docRef.collection("applications").add(appUser).then(function(doc){
                    console.log("nueva app creada", doc)
                    return res.status(200).send({ error:0, response: newapp });
                });
                
            });

		}else{
            return res.status(200).send({ error:1, response: "No existe usuario o no tiene permisos para crear una aplicación." });
        }

	}).catch(function(error) {
		console.log("Error getting document:", error);
        return res.status(200).send({ error:1, response: "Error desconocido al crear la aplicación." });
	});
};


exports.updateLocation = async function(req, res, admin, db) {

    const data = req.body;
    const geohash = require('ngeohash');
    const shortid = require('shortid');
    
    data.geohash = geohash.encode(data.latitude, data.longitude, 12)
    data.geohash5 = geohash.encode(data.latitude, data.longitude, 5)
    data.geohash6 = geohash.encode(data.latitude, data.longitude, 6)
    data.geohash7 = geohash.encode(data.latitude, data.longitude, 7)
    data.geohash8 = geohash.encode(data.latitude, data.longitude, 8)

    const idlocation = shortid.generate();

    var updates = {};
	updates["/"+data.appid+"/"+data.deviceid+"/"+idlocation] = data;
    admin.database().ref().update(updates);
    

    var appRef = db.collection("applications").doc(data.appid);
    const dbCampaigns = appRef.collection("campaigns");

    let listCampaigns = await dbCampaigns.where('estado', '==', 'ACTIVA').get().catch(err => {
        return res.status(200).send({ error:0, response: { mostrarMensaje:false,mensaje:{} } });
    });

    if (listCampaigns.empty) {
        return res.status(200).send({ error:0, response: { mostrarMensaje:false,mensaje:{} } });
    }

    var encontrado = false;
    var ObjCampaign = {};

    console.log("listCampaigns: ",listCampaigns.docs);

    listCampaigns.forEach(doc => {
        console.log(doc.id, '=>', doc.data());
        var dataDoc = doc.data();
        console.log("Recorriendo campaña: ",dataDoc);
        if (dataDoc.geohash != undefined && dataDoc.geohash.includes(data.geohash7)){
            encontrado = true;
            ObjCampaign = dataDoc;
        }

    });
    
    
    if (encontrado){

        console.log("campaña encontrada: ",ObjCampaign);

        var campaign = {
            id:ObjCampaign.id,
            titulo:ObjCampaign.titulo,
            mensaje:ObjCampaign.mensaje,
            imagen: ( (ObjCampaign.imagen!= undefined) ? ObjCampaign.imagen : "" ),
            btnOK:{
                tipo:process.env.TIPO_MENSAJE_VACIO,
                data:""
            }
        }

        if (ObjCampaign.link != undefined){
            campaign.btnOK = {
                tipo: process.env.TIPO_MENSAJE_LINK,
                data: ObjCampaign.link
            }
        }

        const dbCampaign = appRef.collection("campaigns").doc(ObjCampaign.id);
        const dbCampaignsDevicesPrints = dbCampaign.collection("prints").doc(data.deviceid);

        let printDevice = await dbCampaignsDevicesPrints.get();

        if (printDevice.exists){
            console.log("ya se mostró la campaña a este dispositivo", data.deviceid);
            return res.status(200).send({ error:0, response: { mostrarMensaje:false,mensaje:{} } });
        }else{
            dbCampaignsDevicesPrints.set(data).catch(function(error) {
                console.log("Error getting document:", error);
            });
    
            return res.status(200).send({ error:0, response: { mostrarMensaje:true,mensaje:campaign } });
        }

    }else{
        return res.status(200).send({ error:0, response: { mostrarMensaje:false,mensaje:{} } });
    }

};

exports.views = function(req, res, db) {

    const data = req.body;
    data.reg = new Date();

    var appRef = db.collection("applications").doc(data.appid);
    const dbCampaigns = appRef.collection("campaigns").doc(data.idcampaign);
    const dbCampaignsDevices = dbCampaigns.collection("views").doc(data.deviceid);

    return dbCampaignsDevices.set(data).then(function() {
        console.log("nueva vista",data);
        return res.status(200).send({ error:0, response: "" });
    }).catch(function(error) {
        console.log("Error getting document:", error);
        return res.status(200).send({ error:1, response: "algo salió mal" });
    });

};

exports.actions = function(req, res, db) {

    const data = req.body;
    data.reg = new Date();

    var appRef = db.collection("applications").doc(data.appid);
    const dbCampaigns = appRef.collection("campaigns").doc(data.idcampaign);
    const dbCampaignsDevices = dbCampaigns.collection("actions").doc(data.deviceid);

    return dbCampaignsDevices.set(data).then(function() {
        console.log("nueva vista",data);
        return res.status(200).send({ error:0, response: "" });
    }).catch(function(error) {
        console.log("Error getting document:", error);
        return res.status(200).send({ error:1, response: "algo salió mal" });
    });

};

exports.log = function(req, res, db) {

    const data = req.body.data;
    data.reg = new Date();

    return db.collection("logs").add(data).then(function(doc) {
        return res.status(200).send({ error:0, response: "OK" });
	}).catch(function(error) {
		console.log("Error getting document:", error);
        return res.status(200).send({ error:1, response: "Error desconocido al crear la aplicación."+error });
	});

};

exports.search = function(req, res, db,appid) {

    const data = req.body.data;

    var appRef = db.collection("applications").doc(appid);
	return appRef.get().then(function(application) {
		
		if (application.exists) {

            var query = appRef.collection("log");
            Object.keys(data).forEach(function (key) {
                query = query.where(key, "==", data[key]);
            });

            return query.get().then(function(snapshot) {
                if (snapshot.empty) {
                    return res.status(200).send({ error:0, response: [] });
                }

                var geohashObj = {};
                snapshot.forEach(doc => {

                    const nuevo = {
                        geohash:doc.data().geohash,
                        valor:1
                    };

                    if (geohashObj[nuevo.geohash] == undefined) {
                        geohashObj[nuevo.geohash] = nuevo;
                    }else{
                        geohashObj[nuevo.geohash].valor += 1;
                    }

                });

                var list = [];
                Object.keys(geohashObj).forEach(function (key) {
                    list.push(geohashObj[key]);
                });

                return res.status(200).send({ error:0, response: list });

            }).catch(function(error) {
                console.log("Error getting document:", error);
                return res.status(200).send({ error:0, response: [] });
            });

		}else{
            return res.status(200).send({ error:1, response: "No existe aplicación o no tiene permisos para consultar." });
        }

	}).catch(function(error) {
		console.log("Error getting document:", error);
        return res.status(200).send({ error:1, response: "Error desconocido al consultar." });
	});

};


exports.campaign = async function(req, res, db,appid) {

    const data = req.body.data;

    var appRef = db.collection("applications").doc(appid);

    const application = await appRef.get();

    if (application.exists) {
        const geohash = require('ngeohash');
        data.geohash = geohash.encode(data.latitude, data.longitude, 12);
        data.geohash5 = geohash.encode(data.latitude, data.longitude, 5);
        data.geohash6 = geohash.encode(data.latitude, data.longitude, 6);
        data.geohash7 = geohash.encode(data.latitude, data.longitude, 7);
        data.geohash8 = geohash.encode(data.latitude, data.longitude, 8);
        
        if (data.id != undefined){

            data.estado = ((data.estado != undefined) ? data.estado : "ACTIVA" );
            
            const dbCampaigns = appRef.collection("campaigns").doc(data.id);
            let campanaActualizada = await dbCampaigns.update(data);
            console.log("campaña actualizada",campanaActualizada);


            return res.status(200).send({ error:0, response: data });

        }else{

            const shortid = require('shortid');
            data.id = shortid.generate();
            data.reg = new Date();
            data.estado = "ACTIVA"; //PAUSADA //ELIMINADA

            const dbCampaigns = appRef.collection("campaigns").doc(data.id);
            let campanaCreada = await dbCampaigns.set(data);
            console.log("campaña creada",campanaCreada);
            return res.status(200).send({ error:0, response: data });
        }

    }else{
        return res.status(200).send({ error:1, response: "No existe aplicación o no tiene permisos para consultar." });
    }


};