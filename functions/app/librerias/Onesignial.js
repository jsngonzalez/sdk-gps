exports.createApp = function() {

    const request = require('request-promise');
  
    const authorization = ((destino == process.env.PUSH_A_USUARIO)?process.env.HEADER_PUSH_USUARIO:process.env.HEADER_PUSH_CONDUCTOR);
    const appid = ((destino == process.env.PUSH_A_USUARIO)?process.env.APP_PUSH_ID_USUARIO:process.env.APP_PUSH_ID_CONDUCTOR);
  
    objeServicio.app_id = appid;
  
    var options = {
      method: 'POST',
      headers: {
        'Authorization': authorization
      },
      body:objeServicio,
      json: true,
      uri: process.env.URL_PUSH
    };
  
    console.log("se envia push: "+ process.env.URL_PUSH,objeServicio);
    return request(options).then(function (response) {
        console.log(response);
        return null
    }).catch(function (err) {
        console.log('error posting json: ', err);
        return admin.database().ref("/servicios/"+idEmpresa+"/"+idServicio+"/estado").set("PENDIENTE");
    });
    
  };