exports.updateDevice = function(newObj, context, db, admin) {

	console.log("Salvando informacion");

	const appid = context.params.appid;
	const deviceid = context.params.deviceid;
	const idlocation = context.params.idlocation;



	newObj.reg = new Date();
	console.log("updateDevice",newObj);



	const dbLocations = db.collection("applications").doc(appid).collection("devices").doc(deviceid);
	var updates = {};
	updates["/"+appid+"/"+deviceid+"/"+idlocation] = null;
	admin.database().ref().update(updates);

	return dbLocations.set(newObj);

};

exports.incrementGeohash = function(snapshot, context, db) {

	console.log("incrementando geohash");

	const appid = context.params.appid;
	const geohash = snapshot.child('geohash').val();

	console.log("new geohash:",geohash);

	var obj = {
		valor : 1,
		geohash : geohash
	};

	const docRef = db.collection("applications").doc(appid).collection("geohash").doc(obj.geohash);
	return docRef.get().then(function(doc) {
	
		if (doc.exists) {
			console.log("incrementGeohash data:", doc.data());
			const data = doc.data();
			obj.valor = data.valor + 1;
		}

		return docRef.set(obj)

	}).catch(function(error) {
		console.log("Error getting document:", error);
		return docRef.set(obj);
	});
	
};


exports.incrementAttrs = function(docRef,properties,obj) {

	Object.keys(obj).forEach(function (key) {

		if (properties[key] != undefined){

			const dataDB =  properties[key];
			const dataInput =  obj[key][0];

			var list = [];
			var encontrado = false;
			dataDB.forEach(function(element) {
				var nuevo = element;
				//console.log("comparando:",nuevo,dataInput);
				if (nuevo.nombre == dataInput.nombre){
					nuevo.valor = nuevo.valor + dataInput.valor;
					encontrado = true;
					//console.log("encontrado:",nuevo);
				}
				list.push(nuevo);
			});

			if (!encontrado){
				list.push(dataInput);
			}

			properties[key] = list;

			//console.log("nueva propiedad:",key,list);
		}else{
			properties[key]= obj[key];
		}
	});

	return docRef.update({"properties":properties});

}

exports.validAttrs = function(snapshot, context, db, admin, newLocation) {

	console.log("validar atributos");

	const appid = context.params.appid;
	const deviceid = context.params.deviceid;

	const docRef = db.collection("applications").doc(appid);

	const obj = {};
	const newObj = {};
	snapshot.forEach(function(childSnapshot) {
		var key = childSnapshot.key;
		var val = childSnapshot.val();

		obj[key] = [
			{
				nombre: val,
				valor: 1
			}
		];

		newObj[key] = val;

	});

	delete obj.latitude;
	delete obj.longitude;
	delete obj.altitude;
	delete obj.batteryPercentage;
	delete obj.speed;
	delete obj.appid;
	delete obj.deviceid;
	delete obj.geohash;
	delete obj.geohash5;
	delete obj.geohash6;
	delete obj.geohash7;
	delete obj.geohash8;

	return docRef.get().then(function(doc) {
		
		if (doc.exists) {
			const data = doc.data();
			const properties = data.properties;

			if (properties == undefined){
				docRef.update({"properties":obj});
				return newLocation.updateDevice(newObj, context, db, admin);
			}

			return docRef.collection("devices").doc(deviceid).get().then(function(snapshot) {
				if (snapshot.exists) {
					return newLocation.updateDevice(newObj, context, db, admin);
				}else{
					console.log("No existe device", deviceid);
					newLocation.updateDevice(newObj, context, db, admin);
					return newLocation.incrementAttrs(docRef, properties, obj);
				}
			});

		}else{
			console.log("No existe la aplicacion", appid);
			return null;
		}

	});
	
};

exports.log = function(snapshot, context, db, admin) {

	//console.log("Salvando informacion");

	const appid = context.params.appid;
	const deviceid = context.params.deviceid;

	const obj = {};
	snapshot.forEach(function(childSnapshot) {
		var key = childSnapshot.key;
		var val = childSnapshot.val();
		obj[key] = val;
	});

	obj.id = deviceid;
	obj.reg = new Date();

	//console.log("new log:",obj);
	const dbLocations = db.collection("applications").doc(appid).collection("log").doc();
	return dbLocations.set(obj);

};