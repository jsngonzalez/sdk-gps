// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const cors = require('cors')({origin: true});

admin.initializeApp(functions.config().firebase);
var db = admin.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);

process.env.TIPO_MENSAJE_LINK = "LINK";
process.env.TIPO_MENSAJE_VACIO = "";
//https://sdk-gps-981fe.firebaseio.com/app1/device1.json


exports.appnueva = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        if(req.method != "POST" || req.body.data == undefined){
            res.status(404);
        }

        const token = req.get('x-user-token');
        if(token == undefined || token == ""){
            return res.status(200).send({ error:1, response: "No se encontró token" });
        }

        const app = require('./app/servicios/app');
        return app.appnueva(req, res, db, token);

    });
});

exports.newLocation = functions.database.ref('/{appid}/{deviceid}/{idlocation}').onCreate((snapshot, context) => {
    const newLocation = require('./app/realtime/newLocation');
    newLocation.log(snapshot, context, db, admin);
    newLocation.incrementGeohash(snapshot, context, db);
    return newLocation.validAttrs(snapshot, context, db, admin, newLocation);
});

exports.updateLocation = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        if(req.method != "POST" || req.body == undefined){
            res.status(404);
        }

        if(req.body.appid == undefined || req.body.deviceid == undefined){
            return res.status(200).send({ error:1, response: "no se encontro la aplicación o el dispositivo" });
        }

        const app = require('./app/servicios/app');
        return app.updateLocation(req, res, admin, db);

    });
});

exports.views = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        if(req.method != "POST" || req.body == undefined){
            res.status(404);
        }

        if(req.body.appid == undefined || req.body.deviceid == undefined || req.body.idcampaign == undefined){
            return res.status(200).send({ error:1, response: "no se encontro la aplicación o el dispositivo" });
        }

        const app = require('./app/servicios/app');
        return app.views(req, res, db);

    });
});

exports.actions = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        if(req.method != "POST" || req.body == undefined){
            return res.status(404);
        }

        if(req.body.appid == undefined || req.body.deviceid == undefined || req.body.idcampaign == undefined){
            return res.status(200).send({ error:1, response: "no se encontro la aplicación o el dispositivo" });
        }

        const app = require('./app/servicios/app');
        return app.actions(req, res, db);

    });
});

exports.campaign = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        console.log("buscar: ",req.method,req.body.data);
        if(req.method != "POST" || req.body.data == undefined){
            return res.status(404);
        }
        
        const appid = req.get('x-appid');
        if(appid == undefined || appid == ""){
            return res.status(200).send({ error:1, response: "No se encontró token" });
        }

        const app = require('./app/servicios/app');
        return app.campaign(req, res, db, appid);

    });
});

exports.log = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        if(req.method != "POST" || req.body.data == undefined){
            res.status(404);
        }

        const app = require('./app/servicios/app');
        return app.log(req, res, db);

    });
});


exports.search = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        console.log("buscar: ",req.method,req.body.data);
        if(req.method != "POST" || req.body.data == undefined){
            res.status(404);
        }

        const appid = req.get('x-appid');
        if(appid == undefined || appid == ""){
            return res.status(200).send({ error:1, response: "No se encontró token" });
        }

        const app = require('./app/servicios/app');
        return app.search(req, res, db, appid);

    });
});


